export const SELECT_USER = 'SELECT_USER'; 
export const FETCH_USERS = 'FETCH_USERS'; 
export const LOAD_USERS = 'LOAD_USERS';
export const LOAD_TODOS = 'LOAD_TODOS';
export const SAVE_TODO = 'SAVE_TODO';
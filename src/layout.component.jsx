import React from 'react'
import {Link} from 'react-router'

const Layout = (props) => { 
    return <div className="container">
        <div className="row">
            <div className="col-xs-12">
                <Link to="/todos" className="nav-pill" activeClassName="active"> &raquo; Todos </Link>
                <Link to="/users" className="nav-pill" activeClassName="active"> &raquo; Users </Link>

                <h1> Hello Sages!</h1>
                {props.children}
            </div>
        </div>
    </div>
}

export default Layout;
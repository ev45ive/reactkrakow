import Store from '../store'
import {
    LOAD_USERS,
    SELECT_USER,
    LOAD_TODOS,
    SAVE_TODO
} from '../actions.constants'

const appStore = new Store({
    selectedUserId: 1,
    users:[],
    todos:[],
    querySets:[
        {
          id:13, userId:'', user:{}, userTodos:[]
        }
    ]
}, function(state, action){

    switch(action.type){
        case SELECT_USER:
            state.selectedUserId = action.payload.id;
            this.emitChange()
        break;

        case LOAD_USERS:
            this.state.users = action.payload.users;
            this.emitChange();
        break;

        case LOAD_TODOS:
            this.state.selectedUserId = action.payload.userId
            this.state.todos = action.payload.todos;
            this.emitChange();
        break;

        case SAVE_TODO:
           
        break;
    }
})

export default appStore;
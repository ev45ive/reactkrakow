import {createStore, combineReducers, compose} from 'redux'
import DevTools from './DevTools'

import {
    LOAD_USERS,
    SELECT_USER,
    LOAD_TODOS,
    SAVE_TODO
} from './actions.constants'



const users = (users = [], action) => {
    switch(action.type){
        case LOAD_USERS:
            return action.payload.users;
        default:
            return users
    }
}

const todos = (todos = [], action) => {
    switch(action.type){
        case LOAD_TODOS:
            return action.payload.todos;
        default:
            return todos
    }
}

const listsReducer = combineReducers({
    users,
    todos,
    selectedUserId: (x = 1) => x
})

const appReducer = (state = {
    todos: [],
    users:[],
    selectedUserId: 1
}, action) => {

    switch(action.type){
        case SELECT_USER:
            return {
                ...state,
                selectedUserId: action.payload.id
            }
        case LOAD_TODOS:
            return {
                ...state,
                selectedUserId: action.payload.userId   
            };
        default:
            return state
    }
}

const store = createStore((state, action)=>{
    state = listsReducer(state,action)
    state = appReducer(state,action)
    return state
},{
    selectedUserId: 1,
    users:[],
    todos:[],
}, DevTools.instrument())
// [].reduce( (state, next) => ..., initialState )

export default store;
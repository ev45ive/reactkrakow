
import {EventEmitter} from 'events'

class Store extends EventEmitter{
    state = {};

    CHANGE_EVENT = 'change';

    constructor(state, actionHandler){
        super();
        this.state = state;
        this.actionHandler = actionHandler;
    }

    subscribe(callback){
        this.on(this.CHANGE_EVENT, callback)
    }

    emitChange(){
        this.emit(this.CHANGE_EVENT)
    }

    getState(){
        return this.state;
    }

    dispatch(action){
       this.actionHandler(this.state, action)
    }
}

export default Store
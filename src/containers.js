import React from 'react'
import Todos from './todos/todos.component'
import Users from './users/users.component'

import reduxStore from './redux.store'
import actions from './actions'

// const logToken = logDispatcher.register( (action) => {
// })
// logDispatcher.waitFor([storeToken])

export class Provider extends React.Component{

    static childContextTypes = {
        store: React.PropTypes.object,
        actions: React.PropTypes.object
    }

    getChildContext(){
        return {
            store: reduxStore,
            actions: actions
        }
    }

    render(){
        return this.props.children;
    }
}

// function Container(Component, mapProps){
//     return function(props,context){

//     }
// }

// Redux Reselector lib
export const Container = (Component, mapProps) => {

    let ContainerComponent =  (props, context) => {
        console.log(context.store.getState())
        props = mapProps(context.store.getState(), context.actions, props)
        return <Component {...props} />
    }
    
    ContainerComponent.contextTypes = {
        store: React.PropTypes.object,
        actions: React.PropTypes.object
    }
    return ContainerComponent;
}

export const TodosContainer = Container(Todos, (state, actions, props) => ({
    todos: state.todos,
    saveTodo: actions.saveTodo
}))

export const UsersContainer = Container(Users, (state,actions, props)=>({
    users: state.users,
    onSelect: actions.selectUser 
}))
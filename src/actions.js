import dispatcher from './app.dispatcher'
import { 
    LOAD_USERS, 
    SELECT_USER, 
    LOAD_TODOS,
    SAVE_TODO
} from './actions.constants'
import appStore from './stores/app.store'

function fetchUsers(){
    return fetch('http://localhost:3000/users')
    .then(resp => resp.json())
}
function fetchTodos(id){
    return fetch('http://localhost:3000/todos?userId='+id)
    .then(resp => resp.json())
}

function saveTodo(todo){
    return fetch('http://localhost:3000/todos/' + (todo.id || ''),{
        method: todo.id? 'PUT' : 'POST',
        headers:{
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(todo)
    })
    .then(resp => resp.json())
}

const actions = {
    selectUser : ( user ) => {
        dispatcher.dispatch({
            type: SELECT_USER,
            payload: {
                id: user.id
            }
        })
        actions.fetchTodos(user.id)
    },
    fetchUsers: ( ) => {
        fetchUsers()
        .then( users => {
            dispatcher.dispatch({
                type: LOAD_USERS,
                payload:{ users }
            })
        })
    },
    fetchTodos: (id) => {
        fetchTodos(id)
        .then( todos => {
            dispatcher.dispatch({
              type: LOAD_TODOS,
              payload: { todos, userId: id }
            })
        })
    },
    saveTodo: (todo) => {
        let state = appStore.getState();
        let selectedUserId = state.selectedUserId;
        todo.userId = selectedUserId;

        saveTodo(todo)
        .then( (todo) => {
            dispatcher.dispatch({
                type: SAVE_TODO,
                payload: { todo }
            })      
            
            fetchTodos(selectedUserId)
            .then( todos => {
                dispatcher.dispatch({
                type: LOAD_TODOS,
                payload: { todos, userId: selectedUserId }
                })
            })
        })
    }
}

export default actions;
import React from 'react'

const TextContent = (props) => <div id="user-data">
    <div> User Data: </div>
    <div> {props.username}</div>
    <div> {props.email + '!'} </div>
</div>

export default TextContent;
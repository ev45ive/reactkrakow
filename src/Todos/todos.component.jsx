import React from 'react'
import TodoItem from './todo.component'

export default class Todos extends React.Component{

    constructor(){
        super()
        this.state = {
            todos: [ ],
            newTitle: ''
        }
    }


    changeTitle = e => {
        this.setState({
            newTitle: e.target.value
        })
    }

    addTodo = e => {
        this.props.saveTodo({
            title: this.state.newTitle,
            completed: false  
        })
        this.setState({
            newTitle:''
        })
    }

    componentWillMount(){
    }

    componentWillUnmount(){
       // window.confirm('Czy zapisać?')
    }

    // componentWillReceiveProps(props){
    //     if(this.props.user.id != props.user.id){
    //         this.fetchTodos()
    //     }
    // }

    toggle(todo){
        this.props.saveTodo(todo);
    }

    // static PropTypes = {
    //     Item: React.PropTypes.element
    // }

    componentDidUpdate(){
        console.log(this.refs)
        this.refs.todoInput.style.border = '1px solid red'
    }

    render(){
        let Item = TodoItem;

        return <div>
        <button onClick={this.fetchTodos}>Odśwież</button>
            <ul>
                {this.props.todos.map( todo => 
                    <li key={todo.id}>
                        <Item todo={todo}
                         onChange={(todo) => this.toggle(todo)} />    
                    </li>
                )}
            </ul>
            <input type="text" ref="todoInput"
                value={this.state.newTitle}
                onChange={this.changeTitle}
                onKeyUp={ e => e.keyCode == 13 && this.addTodo() } />

            <input type="button" value="Dodaj" onClick={this.addTodo} />
        </div>
    }
}
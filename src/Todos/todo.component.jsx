import React from 'react'

export default class TodoItem extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            todo: props.todo
        }
    }
    
    static defaultProps = {
        todo:{
            title: '',
            completed: false
        }
    }

    // componentWillReceiveProps(){
    //     console.log('props', arguments)
    // }

    toggle(todo){
        this.setState({
            todo: {
                ...this.state.todo,
                completed: !this.state.todo.completed
            }
        },()=>{
            this.props.onChange(this.state.todo);
        })
    }

    render(){
        return <div> 
            {this.state.todo.title}
            <input type="checkbox" 
            checked={this.state.todo.completed}
            onChange={e => this.toggle() } />
        </div> 
    }
}

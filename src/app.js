import React from 'react'
import {Provider, TodosContainer, UsersContainer} from './containers'

import {Route, Router, IndexRoute, IndexRedirect, Redirect, Link, browserHistory} from 'react-router'

import Layout from './layout.component'
const NotFound = (props) => <div> page Not found </div>;

// =====

class App extends React.Component {
    constructor(){
        super();
        this.state = {
            selected: 'todos',
            user: {id:1},
            users: [],
            todos: [],
        }
    }

    componentWillMount(){
        this.props.store.subscribe(()=>{
            let state = this.props.store.getState();

            this.setState({
                user: { 
                    id: state.selectedUserId
                },
                users: state.users,
                todos: state.todos
            })
        })
        this.props.actions.fetchUsers()
    }

    select = (selected) => {
        this.setState({selected})

    }

    render(){
        let activeStyle = {
            borderBottom: '1px solid black'
        }
        let active = this.state.selected;

        return <div>
            <div className="row">
                <div className="col-xs-6">
                <Router history={browserHistory}>
                    <Route path="/" component={Layout} >
                        <IndexRedirect to="todos" />
                        <Route path="todos" component={TodosContainer} />  
                        <Route path="users" component={UsersContainer} />
                    </Route>
                    <Route path="*" component={NotFound} />
                 </Router>
                </div>
            </div>
        </div>
    }
}


export default App;

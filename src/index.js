import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap-theme.css'
import './style.css'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import Layout from './layout.component'
import {Provider} from './containers'

//import appStore from './stores/app.store'
import reduxStore from './redux.store'
import dispatcher from './app.dispatcher';
import actions from './actions'

import DevTools from './DevTools'

const storeToken = dispatcher.register( (action) => {
    reduxStore.dispatch(action)
    console.log('ACTION:',action.type,action)
})

ReactDOM.render(<Provider>
<div>
        <DevTools store={reduxStore}/>
         <App store={reduxStore} actions={actions} />
</div>
</Provider>, document.querySelector('.root'))
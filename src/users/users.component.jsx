import React from 'react'

export default class Users extends React.Component{

    constructor(){
        super()
        this.state = {
            selected: null
        }
    }  

    static propTypes = {
        onSelect: React.PropTypes.func.isRequired
    }

    componentWillMount(){
        
    }

    componentWillUnmount(){
       // window.confirm('Czy zapisać?')
    }

    selectUser(user){
        this.setState({
            selected:user
        })
        this.props.onSelect(user)
    }

    render(){
        return <div>
            <ul>
                {this.props.users.map( user => 
                    <li key={user.id} 
                    onClick={()=>this.selectUser(user)}>
                       {user.name}   
                    </li>
                )}
            </ul>
        </div>
    }
}
var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    entry:[
        path.join(__dirname, 'src/index.js')
    ],
    output:{
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    resolve:{
        extensions: ['','.js','.jsx']
    },
    plugins:[
        new ExtractTextPlugin('styles.css'),
        new HtmlWebpackPlugin({
            template:'./src/index.html',
            filename: 'index.html'
        })
    ],
    devtool:'sourcemap',
    module:{
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel',
                exclude: path.join(__dirname, 'node_modules')
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader','sass-loader']
            },
            ,
		    { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
		    { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }
        ]
    },
	devServer:{
		historyApiFallback: true,
		hot: true,
		port: 80
	}
}
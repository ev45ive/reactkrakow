var config = require('./webpack.config.js')
var webpack = require('webpack')

module.exports = Object.assign({}, config, {

    plugins: config.plugins.concat([
        new webpack.optimize.UglifyJsPlugin()
    ])

})